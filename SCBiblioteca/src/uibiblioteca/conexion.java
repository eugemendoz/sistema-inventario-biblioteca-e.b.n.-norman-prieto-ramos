/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uibiblioteca;

/**
 *
 * @author Administrador
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class conexion {
    
    private final static String db = "biblioteca";
    private final static String login = "root";
    private final static String pass = "";
    private final static String link = "jdbc:mysql://localhost/" + db;

       public conexion() {
           
       }
        
       public static Connection Conectar() {
           Connection conn = null;
           try{
               Class.forName("com.mysql.jdbc.Driver");
               conn = DriverManager.getConnection(link,login,pass);
//               if (conn!=null)
//                   JOptionPane.showMessageDialog(null, "Conexion Exitosa!");
//                  Solo para saber si conecto.
           } catch (ClassNotFoundException | SQLException e){
               String error = "ERROR: " + e.getMessage() + "\nNo se ha podido conectar a la base de Datos BIBLIOTECA";
               JOptionPane.showMessageDialog(null, error);
               System.exit(0);
           }
           return conn;
       }
}