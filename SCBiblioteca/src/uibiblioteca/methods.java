/**
 * Los metodos aca
 * Comenten que hace cada vaina.
 */

package uibiblioteca;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class methods {

    
    public static SimpleDateFormat Y = new SimpleDateFormat("yyyy");
    public static Date Y2 = new Date();
        
    //Verifica si es letra
    public static boolean checkIt(String a){
           if(a.matches("[a-zA-Z]+")){
               return true;
           }else{
               return false;
           }
       }
    
    //Muestra en ventana una string
    public static void ayudaE(Exception a){
           JOptionPane.showMessageDialog(null,a);
       }
    
    public static void ayudaS(String a){
           JOptionPane.showMessageDialog(null,a);
       }
    
    public static boolean AñoBisiesto(String Year){
        
        for(int i = 1900; i< Integer.parseInt(Y.format(Y2)); i += 4){
            if (i == Integer.parseInt(Year)){
                return true;
            }
        }
        return false;
    }
    
    public static void Año(JComboBox A){
        DefaultComboBoxModel CA = new DefaultComboBoxModel();
        //System.out.print(Y.format(Y2));
        for(int i = Integer.parseInt(Y.format(Y2)); i == Integer.parseInt(Y.format(Y2)) ; i --){
            CA.addElement(String.valueOf(i));
            CA.addElement(String.valueOf(i+1));
        }
      A.setModel(CA);
    }
    
    public static void Dias(int Indice,JComboBox A, Boolean B){
        int[] Thosew30 = {3,5,8,10};
        int[] Thosew31 = {0,2,4,6,7,9,11};
        DefaultComboBoxModel CD = new DefaultComboBoxModel();
        
        for(int i = 0; i<Thosew30.length;i++){
            if(Indice == Thosew30[i])
                for(int j = 1;j<=30; j++)
                    CD.addElement(j);
        }
        
        for(int i=0;i<Thosew31.length;i++){
            if(Indice == Thosew31[i])
                for(int j=1;j<=31;j++){
                    CD.addElement(j);
            }
        }
        
        if(Indice == 1 && B){
            for(int i = 1; i<=29;i++){
                CD.addElement(i);
            }
        }
        
        if(Indice == 1 && B == false){
            for(int i = 1; i<=28;i++){
                CD.addElement(i);
            }
        }
        
        A.setModel(CD);
    }
    
    public static void Meses(JComboBox A){
        String[] Months = {"enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
        DefaultComboBoxModel CM = new DefaultComboBoxModel();
        for(int i=0;i<12;i++){
            CM.addElement(Months[i]);
        }
        A.setModel(CM);
    }
    
    //este metodo retorna la primary key de aguna tabla especifica
    public static String ret_cod(String tabla, String atrib){
        String query = null,aux = null,at=null;
        Connection conn = conexion.Conectar();
        Statement sent;
        ResultSet rs;
        try {
             sent = conn.createStatement();
        switch(tabla){
            case "editorial":at="cEditorial"; query= "select cEditorial from editorial where nombre = '"+atrib+ "'"; break;
            case "autor":    at="claveautor";query= "select claveautor from autor where nombre = '"+atrib+ "'"; break;
            case "tema":     at="clavetema";query= "select clavetema from tema where nombre = '"+atrib+ "'"; break;
                
        }//switch
        
        rs = sent.executeQuery(query);
         while(rs.next()){
               aux = rs.getString(at);
                
            }//while
        
        }catch(SQLException e){
            methods.ayudaE(e);
        }
        return aux;
    }
    
    public static String obc_fec(){
        Calendar fecha = Calendar.getInstance();
        fecha.add(Calendar.MONTH, +1);
        String fecha2= Integer.toString(fecha.get(Calendar.DAY_OF_MONTH))+"/"+Integer.toString(fecha.get(Calendar.MONTH))+
                "/"+Integer.toString(fecha.get(Calendar.YEAR));
        return fecha2;
    }     
}//class

